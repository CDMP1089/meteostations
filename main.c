/* 
 * File:   main.c
 * Author: Cristian D. Mart�nez
 *
 * Created on 10 de julio de 2017, 08:39 AM
 */
// PIC24FJ1024GB610 Configuration Bit Settings

// 'C' source line config statements

#include <stdio.h>
#include <stdlib.h>
#include "../nxlib/drivers/time/time.h"
#include "../nxlib/drivers/adc/adc.h"
#include "../nxlib/drivers/serial/serial.h"
#include "../nxlib/libraries/sensors/AM2315/am2315.h"
#include "../nxlib/drivers/RS485/RS485.h"
#include "../nxlib/libraries/sensors/sonometer/sonometer.h"
#include "../nxlib/libraries/sensors/pyranometer/pyranometer.h"
#include "../nxlib/libraries/sensors/level_sensor/level.h"
#include "../nxlib/drivers/timers/timers.h"
#include "../nxlib/drivers/ioc/IOC.h"
#include "system.h"
#include "../nxlib/GlobalVariables.h"
#include "../nxlib/libraries/sensors/gasSensors/gasSensor.h"
#include "../nxlib/libraries/sensors/ArgenSensors/ArgentSensors.h"

#define _XTAL_FREQ 8000000

#include <xc.h>

/*Declaration of station type*/
/* type 1 -> measure: sonometer, wind, temperature, humidity, rain, radiation.
 * type 2 -> measure: wind, temperature, humidity, rain.
 * type 3 -> measure: wind, temperature, humidity, rain, level.
 * type 4 -> measure: sonometer, wind.
 * type 5 -> level.
 * type 6 -> sonometer.
 * type 7 -> gases (CO, NO2, SO2, O3).
 */
#define StationType 2  //

/*Define ADC channels for sensors*/
#define sonometer_CH 8
#define pyranometer_CH 15
#define level_CH 2
#define rain_CH 2


/*Period Time definitions for sensors*/
#define T_Sono  3       //tiempo de lectura para el son�metro
#define T_TH    10      //tiempo de lectura para el termohigrometro
#define T_Pyra  15
#define T_Level 10
#define T_gas   5
#define T_rain  5
#define T_wind  6

float noise, humidity, temperature, light, distance;
extern bool timer_flag;
extern unsigned int seconds, windCount;
extern float windspeed;
extern volatile unsigned char RxBuffer[10];

char trama[100];
char print[50];

uint8_t lastsec, lastsec2, lastsec3, lastsec4, lastsec5, lastsec6; //variables to save current time (in seconds) for each sensor

typedef struct {
    int ID;
    float max;
    float min;
    float value;
    uint16_t counter;
    float Vdata[10];
} sensors;

typedef struct {
    sensors sonometer;
    sensors pyranometer;
    sensors temperature;
    sensors humidity;
    sensors rain;
    sensors windSpeed;
    sensors windDir;
    sensors CO;
    sensors NO2;
    sensors SO2;
    sensors O3;

} Data;


/*functions definitions*/
float max(float *V, int len);
float min(float *V, int len);
float mean(float *V, int len);

Data datos;
unsigned long int c;

/*-------------Main loop--------------*/
int main(int argc, char** argv) {

    SYSTEM_InitializeBoard(); //Board initialization
    //serialInit();
    RS485_Init(9600); // RS485 initialization
    T1_init();
    //IOC_init('B', PIN3, false, true);
    //IOC_init('B', PIN2, true, true);

    /*Initialzation of sensor objects*/
    ClrWdt();
#if ((StationType == 1) || (StationType == 4) || (StationType == 6))
    sonometer_t * sonometer_sensor = sonometer_init(0, sonometer_CH);
    sonometer_begin(sonometer_sensor);
    sensors_event_t sonometer_event = {.type = SENSOR_TYPE_SOUND};
#endif
#if ((StationType == 1) || (StationType == 2) || (StationType == 3))
    am2315_t * am2315_sensor = am2315_init(0, 0);
    am2315_begin(am2315_sensor);
    sensors_event_t am2315_tem_event = {.type = SENSOR_TYPE_AMBIENT_TEMPERATURE};
    sensors_event_t am2315_hum_event = {.type = SENSOR_TYPE_RELATIVE_HUMIDITY};
#endif
#if StationType == 1
    pyranometer_t * pyranometer_sensor = pyranometer_init(0, pyranometer_CH);
    pyranometer_begin(pyranometer_sensor);
    sensors_event_t pyranometer_event = {.type = SENSOR_TYPE_LIGHT};
#endif
#if ((StationType == 5)||(StationType == 3))
    level_t * level_sensor = level_init(0, level_CH);
    level_begin(level_sensor);
    sensors_event_t level_event = {.type = SENSOR_TYPE_PROXIMITY};
#endif
#if ((StationType == 1) || (StationType == 2) || (StationType == 3) || (StationType == 4))
    //initialization for wind  
    windspeed_t * wind_sensor = windspeed_init(0, PIN3);
    windspeed_begin(wind_sensor);
    winddir_t * winddir_sensor = winddir_init(0, rain_CH);
    winddir_begin(winddir_sensor);
    sensors_event_t windspeed_event = {.type = SENSOR_TYPE_SPEED};
    sensors_event_t winddir_event = {.type = SENSOR_TYPE_ORIENTATION};
#endif
#if StationType == 7
    gasSensor_t * gas_sensor = gasSensor_init(0, 1, 9, 10);
    sensors_event_t gasSensor_event = {.type = SENSOR_TYPE_GAS_CONCENTRATION};
    gasSensor_t * gas_sensor2 = gasSensor_init(0, 1, 13, 14);
    sensors_event_t gasSensor_event2 = {.type = SENSOR_TYPE_GAS_CONCENTRATION};
    //initialization for gases
#endif
#if ((StationType == 1) || (StationType == 2) || (StationType == 3))
    //initialization for rain gauge
    rain_t * rain_sensor = rain_init(0,PIN2);
    rain_begin(rain_sensor);
    sensors_event_t rain_event = {.type = SENSOR_TYPE_VOLTAGE};
#endif

    RS485_PutROMString("Iniciado!\n");

    while (1) {
        /*periodical reading for sensors*/

#if ((StationType == 1) || (StationType == 4) || (StationType == 6))
        if (seconds % T_Sono == 0 && lastsec != seconds && !RX1_event) {
            lastsec = seconds;

            sonometer_sensor->get_event(sonometer_sensor, &sonometer_event);
            datos.sonometer.value = sonometer_event.sound;
            datos.sonometer.Vdata[datos.sonometer.counter++] = datos.sonometer.value;
            if (datos.sonometer.counter >= 10)
                datos.sonometer.counter = 0;
            RS485_PutROMString("sononometer read OK\n\r");

            /*uint16_t c;
            c = IOC_get_counter(0);
            Nop();
            Nop();
            Nop();
            datos.windSpeed.value = 2.4*c*T_Sono/60;
            
            datos.rain.value *= IOC_get_counter(1)*0.2794;
             */
        }
#endif
#if StationType == 1
        if (tiempo % T_Pyra == 0 && lastsec2 != tiempo && !RX_event) {
            lastsec2 = tiempo;
            RS485_PutROMString("pyranometer read OK\n\r");
            pyranometer_sensor->get_event(pyranometer_sensor, &pyranometer_event);
            datos.pyranometer.value += pyranometer_event.light;
            datos.pyranometer.counter++;
            ClrWdt();
        }
#endif

#if ((StationType == 1) || (StationType == 2) || (StationType == 3))
        if (seconds % T_TH && lastsec3 != seconds && !RX1_event) {
            lastsec3 = seconds;
            am2315_sensor->get_event(am2315_sensor, &am2315_tem_event);
            am2315_sensor->get_event(am2315_sensor, &am2315_hum_event);
            datos.humidity.value += am2315_hum_event.relative_humidity;
            datos.temperature.value += am2315_tem_event.temperature;
            datos.temperature.counter++;
            datos.humidity.counter++;
            if(datos.temperature.counter > 100){ //this is to avoid the varibles overflow, wheter isn't receive a request message
                datos.humidity.value = 0;
                datos.temperature.value = 0;
                datos.temperature.counter = 0;
                datos.humidity.counter = 0;
            }
            ClrWdt();
        }
#endif


#if ((StationType == 1) || (StationType == 2) || (StationType == 3) || (StationType == 4))
        if (seconds % T_wind == 0 && lastsec4 != seconds && !RX1_event) {
            
            lastsec4 = seconds;
            ClrWdt();
            wind_sensor->get_event(wind_sensor,&windspeed_event);
            winddir_sensor->get_event(winddir_sensor,&winddir_event);
            datos.windSpeed.Vdata[datos.windSpeed.counter++] = windspeed_event.speed/T_wind;
            if(datos.windSpeed.counter >= 10)
                datos.windSpeed.counter=0;
            RS485_PutROMString("wind read!!\n\r");
           
        }
#endif

#if ((StationType == 1) || (StationType == 2) || (StationType == 3))
        if (seconds % T_rain == 0 && lastsec5 != seconds && !RX1_event) {
            lastsec5 = seconds;
            ClrWdt();
            rain_sensor->get_event(rain_sensor, &rain_event);
            datos.rain.value += rain_event.voltage;
                 
        }
#endif

#if ((StationType == 5)||(StationType == 3))
        if (seconds % T_level && lastsec6 != seconds && !RX1_event) {
            lastsec6 = seconds;
        }
#endif

#if (StationType == 7)
        if (seconds % T_gas == 0 && lastsec6 != seconds) {
            lastsec6 = seconds;
            gas_sensor->get_event(gas_sensor, &gasSensor_event);
            gas_sensor2->get_event(gas_sensor2, &gasSensor_event2);
            RS485_PutROMString("reading gas sensor!\n\r");
            //    gas_sensor->get_event(gas_sensor,&gasSensor_event);
            Nop();
            Nop();
            Nop();
            datos.NO2.Vdata[datos.NO2.counter++] = gasSensor_event2.concentration;
            datos.CO.Vdata[datos.CO.counter++] = gasSensor_event.concentration;
            ClrWdt();
        }
        
#endif
        if (RX1_event) { //when a request of data is acepted, the frame is created and send depending of the station type
            switch (StationType) {
                case 1:
                    break;

                case 2: sprintf(trama, "$D%d,%d,%03d,%03d,%03d,%03d,%03d,%03d,%03d\n\r",
                            (ID_Slave-0x30), // data -> $D#ID#,StationType,rain,wind speed (max,min,mean),temp,hum. 
                            StationType,
                            (uint16_t) (datos.rain.value * 10.0),
                            (uint16_t) (mean(datos.windSpeed.Vdata, 10) * 10.0),
                            (uint16_t) (max(datos.windSpeed.Vdata, 10) * 10.0),
                            (uint16_t) (min(datos.windSpeed.Vdata, 10) * 10.0),
                            (uint16_t) (datos.windDir.value ),
                            (uint16_t) ((datos.temperature.value / datos.temperature.counter) * 10.0),
                            (uint16_t) ((datos.humidity.value / datos.humidity.counter) * 10.0));
                        datos.temperature.value = 0;
                        datos.temperature.counter = 0;
                        datos.humidity.value = 0;
                        datos.humidity.counter = 0;
                        RS485_PutROMString(trama);
                        break;

                case 3:
                    break;

                case 4: sprintf(trama, "$D%d,%d,%03d,%03d,%03d,%03d,%03d,%03d,%03d,%03d\n\r",
                            (ID_Slave-0x30), // data -> $D#ID#,StationType,rain,wind speed (max,min,mean),temp,hum. 
                            StationType,
                            (uint16_t) (datos.rain.value * 10.0),
                            (uint16_t) (mean(datos.windSpeed.Vdata, 10) * 10.0),
                            (uint16_t) (max(datos.windSpeed.Vdata, 10) * 10.0),
                            (uint16_t) (min(datos.windSpeed.Vdata, 10) * 10.0),
                            (uint16_t) (datos.windDir.value),
                            (uint16_t) (mean(datos.sonometer.Vdata, sizeof (datos.sonometer.Vdata) / 4) * 10.0),
                            (uint16_t) (max(datos.sonometer.Vdata, sizeof (datos.sonometer.Vdata) / 4) * 10.0),
                            (uint16_t) (min(datos.sonometer.Vdata, sizeof (datos.sonometer.Vdata) / 4) * 10.0));
                        RS485_PutROMString(trama);
                        break;

                case 5:
                    break;

                case 6: sprintf(trama, "$D%d,%d,%03d,%03d,%03d\n\r",
                            (ID_Slave-0x30),
                            StationType,
                            (uint16_t) (mean(datos.sonometer.Vdata, (sizeof (datos.sonometer.Vdata) / 4) * 10.0)),
                            (uint16_t) (max(datos.sonometer.Vdata, (sizeof (datos.sonometer.Vdata) / 4) * 10.0)),
                            (uint16_t) (min(datos.sonometer.Vdata, (sizeof (datos.sonometer.Vdata) / 4) * 10.0)));
                    RS485_PutROMString(trama);
                    break;

                case 7: sprintf(trama, "$D%d,%d,gas,%2.2f,%2.2f,%03d\n\r",
                            (ID_Slave-0x30),
                            StationType,
                            mean(datos.CO.Vdata,sizeof(datos.CO.Vdata)/4),
                            mean(datos.NO2.Vdata,sizeof(datos.NO2.Vdata)/4),
                            (uint16_t) (min(datos.sonometer.Vdata, sizeof (datos.sonometer.Vdata) / 4 * 10.0)));
                        RS485_PutROMString(trama);
                        datos.CO.counter = 0;
                        datos.NO2.counter = 0;
                        break;

                case 8:
                    break;
                default: break;
            }

            RX1_event = false;
            ClrWdt();
        } 
        ClrWdt();
    }
    return (EXIT_SUCCESS);
}

float mean(float *V, int len) {
    float mean;
    int i;
    for (i = 0; i < len; i++) {
        mean += V[i];
    }
    return mean / len;
}

float max(float *V, int len) {
    int i;
    float max;
    max = V[0];
    for (i = 1; i < len; i++) {
        /* If current element of array is greater than max */
        if (V[i] > max) {
            max = V[i];
        }
    }
    return max;
}

float min(float *V, int len) {
    int i;
    float min;
    min = V[0];
    for (i = 1; i < len; i++) {
        if (V[i] < min) {
            min = V[i];
        }
    }
    return min;
}